let express = require('express');
let router = express.Router();
let Joi = require('joi');
let axios = require('axios');

let countries = [];
const country_url = 'https://api.worldbank.org/v2/countries/?format=json&per_page=400';


/*
   Fetch and store countries to server from the external API.
   Data contains unwanted regions, so filter them out by checking if datapoint has capital city defined.
   No real reason to run this all the time / unless a new country is formed.
*/
axios.get(country_url)
    .then(response =>{return response.data[1].filter(country => country.capitalCity);})
    .then(filteredData => countries = filteredData.slice())
    .then(() => console.log("Countries loaded"))
    .catch(err => {
        console.log(err.response.status);
    });

router.get('/country/search', (req, res, next) => {
    // Validate
    if(req.query.q) {
        res.send(searchCountries(req.query.q));
    } else {
        res.status(400).send()
    }
});

router.get('/country/:id/population', (req, res, next) => {

    const parseDataSet = (dataSet) => {
        return {
            label: 'pop',
            data: dataSet.map(data => {
                if(req.query.getDates) {
                    return {year: data.date , value: data.value};
                }
                    return {value: data.value}
            })}
    };

    const schema = Joi.object().keys({
        getDates: Joi.boolean()
    });

    const url = `https://api.worldbank.org/v2/countries/${req.params.id}/indicators/SP.POP.TOTL?format=json`;

    axios.get(url)
        .then(response => {
            Joi.validate(req.query, schema, (err) => {
                if(err) {
                    res.status(400).send()
                } else {
                    res.send(parseDataSet(response.data[1]));
                }
            })
        })
        .catch(err => next(err));
});

router.get('/country/:id/co2emissions', (req, res, next) => {

    const parseDataSet = (dataSet) => {
        return {
            label: 'co2',
            data: dataSet.map(data => {
                if(req.query.getDates) {
                    return {year: data.date , value: data.value};
                }
                    return {value: data.value}
            })}
    };

    const schema = Joi.object().keys({
        getDates: Joi.boolean()
    });

    const url = `https://api.worldbank.org/v2/countries/${req.params.id}/indicators/EN.ATM.CO2E.KT?format=json`;

    axios.get(url)
        .then(response => {
            Joi.validate(req.query, schema, (err) => {
                (err) ? res.status(400).send() : res.send(parseDataSet(response.data[1]));
            })
        })
        .catch( err => next(err));
});

router.get('/country/:name/wiki', (req, res, next) => {

    const url = `https://en.wikipedia.org/api/rest_v1/page/summary/${req.params.name}`;

    // summary text : response.data.extract
    // flag svg: response.data.originalimage.source
    axios.get(url)
        .then(response => res.send(
            {
            label: 'wiki',
            data: {
                summary: response.data.extract,
                flag: response.data.originalimage.source
            },

        }))
        .catch(err => next(err))
});



function searchCountries(query) {

    let pattern = new RegExp('^' + query , 'i');

    let nameContains = country => {
        return pattern.test(country.name) || pattern.test(country.id);
    };

    let parseIdentifiers = country => {
        return [country.name, country.id, country.iso2Code];
    };
    return countries.filter(nameContains).map(parseIdentifiers);
}


module.exports = router;
