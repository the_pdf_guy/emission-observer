# Emission Observer
Web application for searching, displaying and comparing co2 emission data between different countries.

# Node.JS backend
Backend runs on Node.JS and uses Worldbank API for emission data.

# Vanilla JS frontend
For educational purposes I decided to use only vanilla JS on the frontend. I 
quickly realized that coding my own chart program was going to be very time 
consuming and I decided to you ChartJS for displaying the data. In 
hindsight, it would have been better to use more js libraries on this site, but 
nevertheless this project was very instructive and I personally learned a lot
while making it.